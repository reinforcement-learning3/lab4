# Grading

[Link to graded Repo](https://github.zhaw.ch/wernlmax/EVA2Labs/tree/master/lab4)

## Tasks

- [x] Gradient of $`\nabla_\theta log\pi_\theta(a|s)`$ for continous actions
- [x] Policy Gradient Update
- [x] Gradient of $`\nabla_\theta log\pi_\theta(a|s)`$ for discrete actions
- [ ] Hyperparameter tuning
- [x] Fisher Matrix Computation
- [x] Natural Gradient Computation
- [x] Step Size Computation
- [x] Time Dependant Baseline
- [x] TRPO Surrogate Loss Computation
- [x] TRPO KL Divergence Computation
- [x] A2C Advantage Returns
- [x] A2C Loss Computation
- [ ] Testing

## Positive

The quite extensive lab was worked through and completed. All the functions and algorithms necessary for training the agents were implemented.

## Negative

Annotations in the code would be nice to better follow the process. Other than that no Hyperparameter tuning was recorded which considering that the lab was continuing afterwards is understandable. Also in the last testing part only the warm start was used for testing and not for the other two games. Also this i regard as minor since it just shows the algortihm winning different games while one has to wait for the training part.

## Overall grade

Considering the effort necessary to complete this lab I would grade an overall **5.5** since all code sections are completed and running, overshadowing the minor negative points I made above.
