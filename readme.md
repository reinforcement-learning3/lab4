# Lab4

## Policy Gradient

### Gradient computation for continouus actions

The action here follows a multivariate Gaussian distribution with identity covariance matrix.

### Gradient computation for discrete actions

The discrete action space (+1 | -1 force) in the pole balancing environment is given by the categorical distribution of the softmax function. The policy gradient is therefore computed with the softmax gradient as explained in more detail in the link below.

https://slowbreathing.github.io/articles/2019-05/softmax-and-its-gradient

### Natural Gradient

https://kvfrans.com/what-is-the-natural-gradient-and-where-does-it-appear-in-trust-region-policy-optimization/

## Advanced Policy Gradients

### TRPO

https://www.depthfirstlearning.com/2018/TRPO

https://paperswithcode.com/method/

https://spinningup.openai.com/en/latest/algorithms/trpo.html

### Advantage Actor Critic

The actor critic method uses a second critic network (or simply the Temporal Difference Error) to approximate the value function.
trpo
After the agent performs an action the value function can be computed which influences the policy gradient updates of the actor. The critic thereby influences the policy gradient direction and can therefore take part in optimizing the policy.

https://towardsdatascience.com/understanding-actor-critic-methods-931b97b6df3f

https://www.freecodecamp.org/news/an-intro-to-advantage-actor-critic-methods-lets-play-sonic-the-hedgehog-86d6240171d/

### General RL Info

https://mandi-zhao.gitbook.io/deeprl-notes/value-based-methods-1/dqn-and-beyond